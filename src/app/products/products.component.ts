import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Product } from '../core/model/product.model';
import { ProductService } from '../core/services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products: Array<Product> = [];
  public error: string;
  public loading = false;
  public formGroup: FormGroup;

  constructor(private productService: ProductService) { 
    this.formGroup = new FormGroup({
        affiliateId: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  public search() {
    this.loading = true;
    const id = this.formGroup.get("affiliateId").value;
    if (id !== '')  {
      this.productService.getByAffiliateId(id).pipe(
        finalize(
          () => this.loading = false
        )
      ).subscribe(
        (products: Array<Product>) => {
          if (products != null) {
            this.products = products;
          } 
        },
        error => {
          this.error = error.error;
        }
      );
    }
  }

}
