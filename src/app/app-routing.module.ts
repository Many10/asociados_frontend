import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const affiliatesModule = () => import('./affiliates/affiliates.module').then(mod => mod.AffiliatesModule);
const productsModule = () => import('./products/products.module').then(mod => mod.ProductsModule);

const routes: Routes = [
  {
    path: 'app',
    children: [
      {
        path: 'affiliates',
        loadChildren: affiliatesModule
      },
      {
        path: 'products',
        loadChildren: productsModule
      }
    ]
},
{
  path: '',
  redirectTo: '/app/affiliates/index',
  pathMatch: 'full'
},
{
  path: '**',
  redirectTo: '/app/affiliates/index',
  pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
