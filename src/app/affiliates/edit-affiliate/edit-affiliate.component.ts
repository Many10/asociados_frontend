import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Affiliate } from 'src/app/core/model/affiliate.model';
import { AffiliateService } from 'src/app/core/services/affiliate.service';

@Component({
  selector: 'app-edit-affiliate',
  templateUrl: './edit-affiliate.component.html',
  styleUrls: ['./edit-affiliate.component.css']
})
export class EditAffiliateComponent implements OnInit {

  public formGroup: FormGroup;
  public loading = false;
  public error: string;
  @Input()
  public affiliateUpdate: Affiliate;

  @Output()
  public reload = new EventEmitter<boolean>();

  constructor(private affiliateService: AffiliateService) { 
    this.formGroup = new FormGroup({
      name: new FormControl('', Validators.required),
      identification: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      country: new FormControl('', Validators.required)
    });
    this.fillForm();
  }

  ngOnInit() {
      this.fillForm();
  }

  public onSubmit(){
    if (this.affiliateUpdate != null) {
      this.update();
    } 
  }

  public update() {
    const newAffiliate = {
      id: this.affiliateUpdate.id,
      name: this.formGroup.get('name').value,
      identification:this.formGroup.get('identification').value,
      address: this.formGroup.get('address').value,
      country: this.formGroup.get("country").value
    } as Affiliate;
    this.affiliateService.update(newAffiliate).pipe(
      finalize (
        () => {
          return this.loading = false;
        }
      )
    ).subscribe(
      response => {
        this.reload.next(true);
        this.formGroup.reset();
      },
      error => {
        this.error = error;
      }
    );
  }

  public fillForm() {
    if (this.affiliateUpdate != null) {
      this.formGroup = new FormGroup({
        name: new FormControl(this.affiliateUpdate.name, Validators.required),
        identification: new FormControl(this.affiliateUpdate.identification, Validators.required),
        address: new FormControl(this.affiliateUpdate.address, Validators.required),
        country: new FormControl(this.affiliateUpdate.country, Validators.required)
      });
    }
  }
}
