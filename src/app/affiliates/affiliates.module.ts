import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AffiliatesComponent } from './affiliates.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AffiliatesRoutingModule } from './affiliate-routing.module';
import { EditAffiliateComponent } from './edit-affiliate/edit-affiliate.component';

@NgModule({
  declarations: [AffiliatesComponent, EditAffiliateComponent],
  imports: [
    CommonModule,
    AffiliatesRoutingModule,
    ReactiveFormsModule
  ]
})
export class AffiliatesModule { }
