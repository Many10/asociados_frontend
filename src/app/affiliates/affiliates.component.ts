import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Affiliate } from '../core/model/affiliate.model';
import { AffiliateService } from '../core/services/affiliate.service';

@Component({
  selector: 'app-affiliates',
  templateUrl: './affiliates.component.html',
  styleUrls: ['./affiliates.component.css']
})
export class AffiliatesComponent implements OnInit {

  public affiliates: Array<Affiliate> = [];
  public error: string;
  public loading = false;

  public showForm = false;
  public eventsSubject: Subject<boolean> = new Subject<boolean>();
  public affiliateUpdate: Affiliate;

  constructor(private affiliateService: AffiliateService) { 
  }

  ngOnInit() {
    this.getAffiliatesList();
  }

  public getAffiliatesList() {
    this.loading = true;
    this.affiliateService.getAll("").pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (affiliates: Array<Affiliate>) => {
        if (affiliates != null) {
          this.affiliates = affiliates;
        }
      },
      error => {
        this.affiliates = [];
        this.error = error.error;
      }
    );
  }

  public showEditAffiliate() {
    this.showForm = !this.showForm;
  }

  public onReloadEvent(reload: boolean) {
    if (reload) {
      console.log("Recargando");
      this.affiliateUpdate = null;
      this.showForm = false;
      this.getAffiliatesList();
    }
  }

  public onEditAffiliateEvent(affiliate: Affiliate) {
    this.affiliateUpdate = affiliate;
    this.showForm = true;
  }

}
