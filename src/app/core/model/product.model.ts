export class Product {
    id : number;
    accountNumber : string;
    typeProduct : string;
    affiliateId : number;
    affiliateName : string;
    affiliateIdentification : string;
    active : string;
    balance : number;  
}