import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Product } from '../model/product.model';
import { ServiceService } from './service.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends ServiceService<Product> {

  constructor(http: HttpClient) {
    super();
    this.apiUrl = environment.apiProducts;
    this.httpClient = http;
    this.resource = 'Product/';
   }

   public getByAffiliateId(id: string): Observable<Array<Product>> {
    return this.httpClient.get(this.getFullPath() + id).pipe(
        map((res: any) => res)
      );
  }

}
