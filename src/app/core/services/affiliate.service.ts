import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Affiliate } from '../model/affiliate.model';
import { ServiceService } from './service.service';

@Injectable({
  providedIn: 'root'
})
export class AffiliateService  extends ServiceService<Affiliate> {

  constructor(http: HttpClient) {
    super();
    this.httpClient = http;
    this.apiUrl = environment.apiAffiliates;
    this.resource = 'Affiliate/';
   }
   
}
